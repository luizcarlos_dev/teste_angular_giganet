import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {DrawerRoutingModule} from './drawer-routing.module';
import {SharedModule} from '../shared/shared.module';
import {UsersComponent} from './users/users.component';
import {TracksComponent} from './tracks/tracks.component';
import {UsersService} from '../services/api/users.service';
import {DrawerComponent} from './drawer.component';
import {TracksService} from '../services/api/tracks.service';
import {AlbumsService} from '../services/api/albums.service';
import {TracksDetailsDialog} from './tracks/details-dialog/details-dialog';
import {TracksEditDialog} from './tracks/edit-dialog/edit-dialog';
import {TracksCreateDialog} from './tracks/create-dialog/create-dialog';
import {TracksConfirmDeleteDialog} from './tracks/confirm-delete-dialog/confirm-delete-dialog';
import {TracksActionsDialog} from './tracks/actions/actions-dialog';
import {UsersActionsDialog} from './users/actions/actions-dialog';
import {UsersConfirmDeleteDialog} from './users/confirm-delete-dialog/confirm-delete-dialog';
import {UsersEditDialog} from './users/edit-dialog/edit-dialog';
import {UsersDetailsDialog} from './users/details-dialog/details-dialog';
import {UsersCreateDialog} from './users/create-dialog/create-dialog';
import {AlbumsDetailsDialog} from './albums/details-dialog/details-dialog';
import {AlbumsEditDialog} from './albums/edit-dialog/edit-dialog';
import {AlbumsCreateDialog} from './albums/create-dialog/create-dialog';
import {AlbumsConfirmDeleteDialog} from './albums/confirm-delete-dialog/confirm-delete-dialog';
import {AlbumsActionsDialog} from './albums/actions/actions-dialog';
import {AlbumsComponent} from './albums/albums.component';
import {FavoritesComponent} from './favorites/favorites.component';
import {TopComponent} from './top/top.component';
import {FavoritesDetailsDialog} from './favorites/details-dialog/details-dialog';
import {FavoritesConfirmDeleteDialog} from './favorites/confirm-delete-dialog/confirm-delete-dialog';
import {FavoritesCreateDialog} from './favorites/create-dialog/create-dialog';
import {FavoritesActionsDialog} from './favorites/actions/actions-dialog';

@NgModule({
  entryComponents: [
    TracksDetailsDialog,
    TracksEditDialog,
    TracksCreateDialog,
    TracksConfirmDeleteDialog,
    TracksActionsDialog,
    UsersDetailsDialog,
    UsersEditDialog,
    UsersCreateDialog,
    UsersConfirmDeleteDialog,
    UsersActionsDialog,
    AlbumsDetailsDialog,
    AlbumsEditDialog,
    AlbumsCreateDialog,
    AlbumsConfirmDeleteDialog,
    AlbumsActionsDialog,
    FavoritesDetailsDialog,
    FavoritesCreateDialog,
    FavoritesConfirmDeleteDialog,
    FavoritesActionsDialog
  ],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DrawerRoutingModule
  ],
  providers: [
    UsersService,
    TracksService,
    AlbumsService,
  ],
  declarations: [
    TracksDetailsDialog,
    TracksEditDialog,
    TracksCreateDialog,
    TracksConfirmDeleteDialog,
    TracksActionsDialog,
    UsersDetailsDialog,
    UsersEditDialog,
    UsersCreateDialog,
    UsersConfirmDeleteDialog,
    UsersActionsDialog,
    UsersComponent,
    AlbumsDetailsDialog,
    AlbumsEditDialog,
    AlbumsCreateDialog,
    AlbumsConfirmDeleteDialog,
    AlbumsActionsDialog,
    AlbumsComponent,
    FavoritesDetailsDialog,
    FavoritesCreateDialog,
    FavoritesConfirmDeleteDialog,
    FavoritesActionsDialog,
    TracksComponent,
    DrawerComponent,
    FavoritesComponent,
    TopComponent
  ]
})

export class DrawerModule {
}
