import {Component, OnInit} from '@angular/core';
import {ProgressService} from '../../services/app/progress.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {Album} from '../../model/album';
import {Track} from '../../model/track';
import {AlbumsService} from '../../services/api/albums.service';
import {TracksService} from '../../services/api/tracks.service';

@Component({
  selector: 'app-top',
  providers: [],
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {

  constructor(private _albumsService: AlbumsService,
              private _tracksService: TracksService,
              private _router: Router,
              private _dialog: MatDialog,
              private _snackBar: MatSnackBar,
              private _progressService: ProgressService
  ) {
  }

  public tracks: Track[];
  public albums: Album[];

  ngOnInit() {
    this._getAlbums();
  }

  private _getAlbums() {
    this._progressService.openProgressDialog();

    this._albumsService.findAll()
      .subscribe(
        res => {
          this.albums = res.data;
          this._progressService.closeProgressDialog();

          this._getTracks();
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });

          this._getTracks();

        }
      );
  }

  private _getTracks() {
    this._progressService.openProgressDialog();

    this._tracksService.findAll()
      .subscribe(
        res => {
          this.tracks = res.data;
          this._progressService.closeProgressDialog();
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

}
