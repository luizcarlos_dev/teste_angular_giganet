import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';
import {Track} from '../../../model/track';
import {Album} from '../../../model/album';

@Component({
  selector: 'app-favorites-details-dialog',
  templateUrl: './details-dialog.html',
  styleUrls: ['./details-dialog.scss']
})
export class FavoritesDetailsDialog implements OnInit {

  public favorite: Track | Album;

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.favorite = {...this.data.favorite};
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
