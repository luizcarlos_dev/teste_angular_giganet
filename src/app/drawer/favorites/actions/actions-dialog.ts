import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';

@Component({
  selector: 'app-favorites-actions-dialog',
  templateUrl: './actions-dialog.html',
  styleUrls: ['./actions-dialog.scss']
})
export class FavoritesActionsDialog implements OnInit {

  public INFO = 'INFO';
  public EDIT = 'EDIT';
  public DELETE = 'DELETE';

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
