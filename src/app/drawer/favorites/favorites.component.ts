import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../services/api/users.service';
import {ProgressService} from '../../services/app/progress.service';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {Track} from '../../model/track';
import {Album} from '../../model/album';
import {AuthorizationStorageService} from '../../services/app/storage/authorization.storage';
import {FavoritesDetailsDialog} from './details-dialog/details-dialog';
import {FavoritesConfirmDeleteDialog} from './confirm-delete-dialog/confirm-delete-dialog';
import {FavoritesCreateDialog} from './create-dialog/create-dialog';
import {FavoritesActionsDialog} from './actions/actions-dialog';
import {User} from '../../model/user';

@Component({
  selector: 'app-favorites',
  providers: [],
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  private _detailsDialogRef: MatDialogRef<FavoritesDetailsDialog>;
  private _confirmDeleteDialogRef: MatDialogRef<FavoritesConfirmDeleteDialog>;
  private _createDialogRef: MatDialogRef<FavoritesCreateDialog>;
  private _actionsDialogRef: MatDialogRef<FavoritesActionsDialog>;

  constructor(private _usersService: UsersService,
              private _router: Router,
              private _authorizationStorageService: AuthorizationStorageService,
              private _dialog: MatDialog,
              private _snackBar: MatSnackBar,
              private _progressService: ProgressService
  ) {
  }

  public user: User;
  public tracks: Track[];
  public albums: Album[];
  public searchLike = '';

  ngOnInit() {
    this._getFavorites();
  }

  search() {
    if (this.searchLike.length % 4 === 0) {
      this._getFavorites();
    }
  }

  private _getFavorites() {
    this._progressService.openProgressDialog();

    const authorization = this._authorizationStorageService.getAuthorization();

    this._usersService.findAll('where={"email": "' + authorization.email + '"}')
      .subscribe(
        res => {

          this.user = res.data[0];

          if (this.user.favorites.tracks) {
            this.tracks = this.user.favorites.tracks;
          }

          if (this.user.favorites.albums) {
            this.albums = this.user.favorites.albums;
          }

          this._progressService.closeProgressDialog();
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _deleteFavorite(deletedFavorite, type) {

    if (type === 'track') {
      for (const track of this.user.favorites.tracks) {
        if (track._id === deletedFavorite._id) {
          const index = this.user.favorites.tracks.indexOf(track);
          this.user.favorites.tracks.splice(index, 1);
        }
      }
    } else if (type === 'album') {
      for (const album of this.user.favorites.albums) {
        if (album._id === deletedFavorite._id) {
          const index = this.user.favorites.albums.indexOf(album);
          this.user.favorites.albums.splice(index, 1);
        }
      }
    }

    this._progressService.openProgressDialog();

    this._usersService.update(this.user, this.user._id)
      .subscribe(
        res => {
          this._snackBar.open('Item removido aos favoritos com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getFavorites();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );

  }

  private _createFavorite(newFavorite, type) {
    const auxUser = {...this.user};
    let newUser;

    if (type === 'track') {

      let newTracks: any[];
      newTracks = auxUser.favorites.tracks ? auxUser.favorites.tracks : [];
      newTracks.push(newFavorite[0]);

      newUser = {
        ...auxUser,
        favorites :
          {
            tracks: newTracks,
            albums: auxUser.favorites.albums
          }
      };

    } else if (type === 'album') {

      let newAlbums: any[];
      newAlbums = auxUser.favorites.albums ? auxUser.favorites.albums : [];

      newAlbums.push(newFavorite[0]);

      newUser = {
        ...auxUser,
        favorites :
          {
            tracks: auxUser.favorites.tracks,
            albums: newAlbums
          }
      };

    }

    this._progressService.openProgressDialog();

    this._usersService.update(newUser, newUser._id)
      .subscribe(
        res => {
          this._snackBar.open('Item Adicionado aos favoritos com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getFavorites();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );

  }

  openFavoritesDetailsDialog(favorite, type) {
    this._detailsDialogRef = this._dialog.open(FavoritesDetailsDialog, {
      disableClose: true,
      data: { favorite: favorite, type: type}
    });
  }

  openFavoritesConfirmDeleteDialog(favorite, type) {
    this._confirmDeleteDialogRef = this._dialog.open(FavoritesConfirmDeleteDialog, {
      disableClose: true,
      data: { favorite: favorite, type: type}
    });
    this._confirmDeleteDialogRef.afterClosed().subscribe(deletedFavorite => {
      if (deletedFavorite) {
        this._deleteFavorite(deletedFavorite, type);
      }
    });
  }

  openFavoritesCreateDialog() {
    this._createDialogRef = this._dialog.open(FavoritesCreateDialog, {
      disableClose: true,
      data: {}
    });
    this._createDialogRef.afterClosed().subscribe( data => {
      if (data.favorite) {
        const newFavorite = {
          ...data.favorite
        };
        this._createFavorite(newFavorite, data.type);
      }
    });
  }

  openFavoritesActionDialog(favorite, type) {
    this._actionsDialogRef = this._dialog.open(FavoritesActionsDialog, {
      disableClose: true,
      data: { favorite: favorite, type: type}
    });
    this._actionsDialogRef.afterClosed().subscribe(action => {
      switch (action) {
        case 'INFO': {
          this.openFavoritesDetailsDialog(favorite, type);
          break;
        }
        case 'DELETE': {
          this.openFavoritesConfirmDeleteDialog(favorite, type);
          break;
        }
      }
    });
  }

}
