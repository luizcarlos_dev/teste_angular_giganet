import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';
import {Track} from '../../../model/track';
import {Album} from '../../../model/album';
import {TracksService} from '../../../services/api/tracks.service';
import {ProgressService} from '../../../services/app/progress.service';
import {AlbumsService} from '../../../services/api/albums.service';

@Component({
  selector: 'app-this.data.favorites-create-dialog',
  templateUrl: './create-dialog.html',
  styleUrls: ['./create-dialog.scss']
})
export class FavoritesCreateDialog implements OnInit {

  public tracks: Track[];
  public albums: Album[];

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              private _tracksService: TracksService,
              private _albumsService: AlbumsService,
              private _snackBar: MatSnackBar,
              private _progressService: ProgressService,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    this.data.favorite = {};

    this.tracks = [];
    this._getTracks();

  }

  private _getTracks() {
    this._progressService.openProgressDialog();

    this._tracksService.findAll()
      .subscribe(
        res => {
          this.tracks = res.data;
          this._progressService.closeProgressDialog();


          this.albums = [];
          this._getAlbums();

        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });


          this.albums = [];
          this._getAlbums();

        }
      );
  }

  private _getAlbums() {
    this._progressService.openProgressDialog();

    this._albumsService.findAll()
      .subscribe(
        res => {
          this.albums = res.data;
          this._progressService.closeProgressDialog();
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
