import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';
import {Album} from '../../../model/album';
import {Track} from '../../../model/track';

@Component({
  selector: 'app-favorites-confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.html',
  styleUrls: ['./confirm-delete-dialog.scss']
})
export class FavoritesConfirmDeleteDialog implements OnInit {

  public favorite: Track | Album;

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    this.favorite = {...this.data.favorite};
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
