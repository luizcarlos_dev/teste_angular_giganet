import {Component, OnInit} from '@angular/core';
import {TracksService} from '../../services/api/tracks.service';
import {ProgressService} from '../../services/app/progress.service';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {Track} from '../../model/track';
import {TracksDetailsDialog} from './details-dialog/details-dialog';
import {TracksConfirmDeleteDialog} from './confirm-delete-dialog/confirm-delete-dialog';
import {TracksEditDialog} from './edit-dialog/edit-dialog';
import {TracksCreateDialog} from './create-dialog/create-dialog';
import {TracksActionsDialog} from './actions/actions-dialog';

@Component({
  selector: 'app-tracks',
  providers: [],
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.scss']
})
export class TracksComponent implements OnInit {

  private _baseLimit = 10;
  private _confirmDeleteDialogRef: MatDialogRef<TracksConfirmDeleteDialog>;
  private _editDialogRef: MatDialogRef<TracksEditDialog>;
  private _createDialogRef: MatDialogRef<TracksCreateDialog>;
  private _detailsDialogRef: MatDialogRef<TracksDetailsDialog>;
  private _actionsDialogRef: MatDialogRef<TracksActionsDialog>;

  public tracks: Track[];
  public offset = 0;
  public limit = this._baseLimit;

  constructor(private _tracksService: TracksService,
              private _router: Router,
              private _dialog: MatDialog,
              private _snackBar: MatSnackBar,
              private _progressService: ProgressService
  ) {
  }

  ngOnInit() {
    this._getTracks();
  }

  public onPageChange(event) {
    this.offset = event.pageIndex * this._baseLimit;
    this.limit = this._baseLimit + event.pageIndex * this._baseLimit;
    this._getTracks();
  }

  private _getTracks() {
    this._progressService.openProgressDialog();

    this._tracksService.findAll('offset=' + this.offset + '&' + 'limit=' + this.limit)
      .subscribe(
        res => {
          this.tracks = res.data;
          this._progressService.closeProgressDialog();
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _createTrack(track: Track) {
    this._progressService.openProgressDialog();

    this._tracksService.create(track)
      .subscribe(
        res => {
          this._snackBar.open('Nova música criada com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getTracks();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _updateTrack(track: Track) {
    this._progressService.openProgressDialog();
    this._tracksService.update(track, track._id)
      .subscribe(
        res => {
          this._snackBar.open('Nova música atualizada com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getTracks();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _deleteTrack(track: Track) {
    this._progressService.openProgressDialog();
    this._tracksService.remove(track._id)
      .subscribe(
        res => {
          this._snackBar.open('Música deletada com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getTracks();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  openTracksDetailsDialog(track) {
    this._detailsDialogRef = this._dialog.open(TracksDetailsDialog, {
      disableClose: true,
      data: { track: track}
    });
  }

  openTracksConfirmDeleteDialog(track) {
    this._confirmDeleteDialogRef = this._dialog.open(TracksConfirmDeleteDialog, {
      disableClose: true,
      data: { track: track}
    });
    this._confirmDeleteDialogRef.afterClosed().subscribe(deletedTrack => {
      if (deletedTrack) {
        this._deleteTrack(deletedTrack);
      }
    });
  }

  openTracksEditDialog(track) {
    this._editDialogRef = this._dialog.open(TracksEditDialog, {
      disableClose: true,
      data: { track: track}
    });
    this._editDialogRef.afterClosed().subscribe(newTrack => {
      if (newTrack) {
        this._updateTrack(newTrack);
      }
    });
  }

  openTracksCreateDialog() {
    this._createDialogRef = this._dialog.open(TracksCreateDialog, {
      disableClose: true,
      data: {}
    });
    this._createDialogRef.afterClosed().subscribe(track => {
      if (track) {
        this._createTrack(track);
      }
    });
  }

  openTracksActionDialog(track) {
    this._actionsDialogRef = this._dialog.open(TracksActionsDialog, {
      disableClose: true,
      data: { track: track}
    });
    this._actionsDialogRef.afterClosed().subscribe(action => {
      switch (action) {
        case 'INFO': {
          this.openTracksDetailsDialog(track);
          break;
        }
        case 'EDIT': {
          this.openTracksEditDialog(track);
          break;
        }
        case 'DELETE': {
          this.openTracksConfirmDeleteDialog(track);
          break;
        }
      }
    });
  }

}
