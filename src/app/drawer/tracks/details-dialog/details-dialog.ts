import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';

@Component({
  selector: 'app-tracks-details-dialog',
  templateUrl: './details-dialog.html',
  styleUrls: ['./details-dialog.scss']
})
export class TracksDetailsDialog implements OnInit {

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
