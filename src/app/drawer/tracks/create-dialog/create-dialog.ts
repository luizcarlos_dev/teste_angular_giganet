import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';
import {MyErrorStateMatcher} from '../../../../my-error-state-matcher';
import {FormControl, Validators} from '@angular/forms';
import {Track} from '../../../model/track';

@Component({
  selector: 'app-tracks-create-dialog',
  templateUrl: './create-dialog.html',
  styleUrls: ['./create-dialog.scss']
})
export class TracksCreateDialog implements OnInit {

  public matcher = new MyErrorStateMatcher();
  public nameFormControl = new FormControl('', [
    Validators.required
  ]);
  public durationFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]{0,10}$')
  ]);

  public track: Track;

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    this.track = new Track();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
