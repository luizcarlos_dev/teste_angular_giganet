import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-drawer',
  providers: [],
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent implements OnInit {

  constructor(private _router: Router){
  }

  ngOnInit() {
  }

  routerNavigate(route: string) {
    this._router.navigate([route]);
  }
}
