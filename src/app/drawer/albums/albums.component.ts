import {Component, OnInit} from '@angular/core';
import {AlbumsService} from '../../services/api/albums.service';
import {ProgressService} from '../../services/app/progress.service';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {Album} from '../../model/album';
import {AlbumsDetailsDialog} from './details-dialog/details-dialog';
import {AlbumsConfirmDeleteDialog} from './confirm-delete-dialog/confirm-delete-dialog';
import {AlbumsEditDialog} from './edit-dialog/edit-dialog';
import {AlbumsCreateDialog} from './create-dialog/create-dialog';
import {AlbumsActionsDialog} from './actions/actions-dialog';
import * as moment from 'moment';
import 'moment/locale/pt-br';

moment.locale('pt-BR');

@Component({
  selector: 'app-albums',
  providers: [],
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {

  private _baseLimit = 10;
  private _confirmDeleteDialogRef: MatDialogRef<AlbumsConfirmDeleteDialog>;
  private _editDialogRef: MatDialogRef<AlbumsEditDialog>;
  private _createDialogRef: MatDialogRef<AlbumsCreateDialog>;
  private _detailsDialogRef: MatDialogRef<AlbumsDetailsDialog>;
  private _actionsDialogRef: MatDialogRef<AlbumsActionsDialog>;

  public albums: Album[];
  public offset = 0;
  public limit = this._baseLimit;

  constructor(private _albumsService: AlbumsService,
              private _router: Router,
              private _dialog: MatDialog,
              private _snackBar: MatSnackBar,
              private _progressService: ProgressService
  ) {
  }

  ngOnInit() {
    this._getAlbums();
  }

  public onPageChange(event) {
    this.offset = event.pageIndex * this._baseLimit;
    this.limit = this._baseLimit + event.pageIndex * this._baseLimit;
    this._getAlbums();
  }

  private _getAlbums() {
    this._progressService.openProgressDialog();

    this._albumsService.findAll('offset=' + this.offset + '&' + 'limit=' + this.limit)
      .subscribe(
        res => {
          this.albums = res.data;
          this._progressService.closeProgressDialog();
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _createAlbum(album: Album) {
    this._progressService.openProgressDialog();
    this._albumsService.create(album)
      .subscribe(
        res => {
          this._snackBar.open('Novo album criado com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getAlbums();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _updateAlbum(album: Album) {
    this._progressService.openProgressDialog();
    this._albumsService.update(album, album._id)
      .subscribe(
        res => {
          this._snackBar.open('Album atualizado com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getAlbums();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _deleteAlbum(album: Album) {
    this._progressService.openProgressDialog();
    this._albumsService.remove(album._id)
      .subscribe(
        res => {
          this._snackBar.open('Album deletado com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getAlbums();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  openAlbumsDetailsDialog(album) {
    this._detailsDialogRef = this._dialog.open(AlbumsDetailsDialog, {
      disableClose: true,
      data: { album: album}
    });
  }

  openAlbumsConfirmDeleteDialog(album) {
    this._confirmDeleteDialogRef = this._dialog.open(AlbumsConfirmDeleteDialog, {
      disableClose: true,
      data: { album: album}
    });
    this._confirmDeleteDialogRef.afterClosed().subscribe(deletedAlbum => {
      if (deletedAlbum) {
        this._deleteAlbum(deletedAlbum);
      }
    });
  }

  openAlbumsEditDialog(album) {
    this._editDialogRef = this._dialog.open(AlbumsEditDialog, {
      disableClose: true,
      data: { album: album}
    });
    this._editDialogRef.afterClosed().subscribe(res => {
      if (res) {
        const updatedAlbum = {
          ...res
        };
        this._updateAlbum(updatedAlbum);
      }
    });
  }

  openAlbumsCreateDialog() {
    this._createDialogRef = this._dialog.open(AlbumsCreateDialog, {
      disableClose: true,
      data: {}
    });
    this._createDialogRef.afterClosed().subscribe(album => {
      if (album) {
        const newAlbum = {
          ...album
        };
        this._createAlbum(newAlbum);
      }
    });
  }

  openAlbumsActionDialog(album) {
    this._actionsDialogRef = this._dialog.open(AlbumsActionsDialog, {
      disableClose: true,
      data: { album: album}
    });
    this._actionsDialogRef.afterClosed().subscribe(action => {
      switch (action) {
        case 'INFO': {
          this.openAlbumsDetailsDialog(album);
          break;
        }
        case 'EDIT': {
          this.openAlbumsEditDialog(album);
          break;
        }
        case 'DELETE': {
          this.openAlbumsConfirmDeleteDialog(album);
          break;
        }
      }
    });
  }

}
