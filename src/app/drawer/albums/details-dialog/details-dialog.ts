import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import {Album} from '../../../model/album';

moment.locale('pt-BR');

@Component({
  selector: 'app-albums-details-dialog',
  templateUrl: './details-dialog.html',
  styleUrls: ['./details-dialog.scss']
})
export class AlbumsDetailsDialog implements OnInit {

  public album: Album;

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.album = {
      ...this.data.album,
      birthDate: moment(this.data.album.birthDate).format('LL')
    };
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
