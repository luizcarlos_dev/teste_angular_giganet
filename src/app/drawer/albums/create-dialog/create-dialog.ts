import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';
import {MyErrorStateMatcher} from '../../../../my-error-state-matcher';
import {FormControl, Validators} from '@angular/forms';
import {Album} from '../../../model/album';
import {ProgressService} from '../../../services/app/progress.service';
import {TracksService} from '../../../services/api/tracks.service';
import {Track} from '../../../model/track';

@Component({
  selector: 'app-albums-create-dialog',
  templateUrl: './create-dialog.html',
  styleUrls: ['./create-dialog.scss']
})
export class AlbumsCreateDialog implements OnInit {

  public matcher = new MyErrorStateMatcher();
  public nameFormControl = new FormControl('', [
    Validators.required
  ]);
  public artistFormControl = new FormControl('', [
    Validators.required
  ]);
  public yearFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]{0,10}$')
  ]);

  public album: Album;
  public tracks: Track[];

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              private _tracksService: TracksService,
              private _snackBar: MatSnackBar,
              private _progressService: ProgressService,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    this.album = new Album();
    this.album.tracks = [];
    this._getTracks();
  }

  public addTrack() {
    this.album.tracks.push(new Track());
  }

  private _getTracks() {
    this._progressService.openProgressDialog();

    this._tracksService.findAll()
      .subscribe(
        res => {
          this.tracks = res.data;
          this._progressService.closeProgressDialog();
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
