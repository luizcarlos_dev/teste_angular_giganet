import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DrawerComponent} from './drawer.component';
import {UsersComponent} from './users/users.component';
import {TracksComponent} from './tracks/tracks.component';
import {AlbumsComponent} from './albums/albums.component';
import {FavoritesComponent} from './favorites/favorites.component';
import {TopComponent} from './top/top.component';

const routes: Routes = [
  {
    path: '',
    component: DrawerComponent,
    children: [
      {
        path: '',
        redirectTo: 'tracks',
        pathMatch: 'full'
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path: 'albums',
        component: AlbumsComponent
      },
      {
        path: 'tracks',
        component: TracksComponent
      },
      {
        path: 'favorites',
        component: FavoritesComponent
      },
      {
        path: 'top',
        component: TopComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DrawerRoutingModule {
}
