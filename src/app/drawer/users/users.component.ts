import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../services/api/users.service';
import {ProgressService} from '../../services/app/progress.service';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {User} from '../../model/user';
import {UsersDetailsDialog} from './details-dialog/details-dialog';
import {UsersConfirmDeleteDialog} from './confirm-delete-dialog/confirm-delete-dialog';
import {UsersEditDialog} from './edit-dialog/edit-dialog';
import {UsersCreateDialog} from './create-dialog/create-dialog';
import {UsersActionsDialog} from './actions/actions-dialog';
import * as crypto from 'crypto-js';
import * as moment from 'moment';
import 'moment/locale/pt-br';

moment.locale('pt-BR');

@Component({
  selector: 'app-users',
  providers: [],
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  private _baseLimit = 10;
  private _confirmDeleteDialogRef: MatDialogRef<UsersConfirmDeleteDialog>;
  private _editDialogRef: MatDialogRef<UsersEditDialog>;
  private _createDialogRef: MatDialogRef<UsersCreateDialog>;
  private _detailsDialogRef: MatDialogRef<UsersDetailsDialog>;
  private _actionsDialogRef: MatDialogRef<UsersActionsDialog>;

  public users: User[];
  public offset = 0;
  public limit = this._baseLimit;

  constructor(private _usersService: UsersService,
              private _router: Router,
              private _dialog: MatDialog,
              private _snackBar: MatSnackBar,
              private _progressService: ProgressService
  ) {
  }

  ngOnInit() {
    this._getUsers();
  }

  public onPageChange(event) {
    this.offset = event.pageIndex * this._baseLimit;
    this.limit = this._baseLimit + event.pageIndex * this._baseLimit;
    this._getUsers();
  }

  private _getUsers() {
    this._progressService.openProgressDialog();

    this._usersService.findAll('offset=' + this.offset + '&' + 'limit=' + this.limit)
      .subscribe(
        res => {
          this.users = res.data;
          this._progressService.closeProgressDialog();
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _createUser(user: User) {
    this._progressService.openProgressDialog();

    this._usersService.create(user)
      .subscribe(
        res => {
          this._snackBar.open('Novo usuário criado com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getUsers();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _updateUser(user: User) {
    this._progressService.openProgressDialog();
    this._usersService.update(user, user._id)
      .subscribe(
        res => {
          this._snackBar.open('Usuário atualizado com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getUsers();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  private _deleteUser(user: User) {
    this._progressService.openProgressDialog();
    this._usersService.remove(user._id)
      .subscribe(
        res => {
          this._snackBar.open('Usuário deletado com sucesso!', 'OK', {
            duration: 5000,
          });
          this._progressService.closeProgressDialog();

          this._getUsers();
        },
        err => {
          this._progressService.closeProgressDialog();
          this._snackBar.open('Ocorreu um erro inesperado!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }

  openUsersDetailsDialog(user) {
    this._detailsDialogRef = this._dialog.open(UsersDetailsDialog, {
      disableClose: true,
      data: { user: user}
    });
  }

  openUsersConfirmDeleteDialog(user) {
    this._confirmDeleteDialogRef = this._dialog.open(UsersConfirmDeleteDialog, {
      disableClose: true,
      data: { user: user}
    });
    this._confirmDeleteDialogRef.afterClosed().subscribe(deletedUser => {
      if (deletedUser) {
        this._deleteUser(deletedUser);
      }
    });
  }

  openUsersEditDialog(user) {
    this._editDialogRef = this._dialog.open(UsersEditDialog, {
      disableClose: true,
      data: { user: user}
    });
    this._editDialogRef.afterClosed().subscribe(res => {
      if (res) {
        const updatedUser = {
          ...res,
          password: crypto.SHA256(res.password).toString(crypto.enc.Base64),
          birthDate: moment(
            new Date( res.birthDate[4] +  res.birthDate[5] + res.birthDate[6] + res.birthDate[7],
              res.birthDate[2] +  res.birthDate[3] - 1, res.birthDate[0] + res.birthDate[1])
          ).format('YYYY-MM-DD')
        };
        this._updateUser(updatedUser);
      }
    });
  }


  openUsersCreateDialog() {
    this._createDialogRef = this._dialog.open(UsersCreateDialog, {
      disableClose: true,
      data: {}
    });
    this._createDialogRef.afterClosed().subscribe(user => {
      if (user) {
        const newUser = {
          ...user,
          password: crypto.SHA256(user.password).toString(crypto.enc.Base64),
          birthDate: moment(
            new Date( user.birthDate[4] +  user.birthDate[5] + user.birthDate[6] + user.birthDate[7],
              user.birthDate[2] +  user.birthDate[3] - 1, user.birthDate[0] + user.birthDate[1])
          ).format('YYYY-MM-DD')
        };
        this._createUser(newUser);
      }
    });
  }

  openUsersActionDialog(user) {
    this._actionsDialogRef = this._dialog.open(UsersActionsDialog, {
      disableClose: true,
      data: { user: user}
    });
    this._actionsDialogRef.afterClosed().subscribe(action => {
      switch (action) {
        case 'INFO': {
          this.openUsersDetailsDialog(user);
          break;
        }
        case 'EDIT': {
          this.openUsersEditDialog(user);
          break;
        }
        case 'DELETE': {
          this.openUsersConfirmDeleteDialog(user);
          break;
        }
      }
    });
  }

}
