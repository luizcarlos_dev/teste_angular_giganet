import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import {User} from '../../../model/user';

moment.locale('pt-BR');

@Component({
  selector: 'app-users-confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.html',
  styleUrls: ['./confirm-delete-dialog.scss']
})
export class UsersConfirmDeleteDialog implements OnInit {

  public user: User;

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    this.user = {
      ...this.data.user,
      birthDate: moment(this.data.user.birthDate).format('LL')
    };
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
