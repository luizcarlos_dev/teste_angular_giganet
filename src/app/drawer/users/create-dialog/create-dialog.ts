import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProgressDialog} from '../../../progress-dialog/progress-dialog';
import {MyErrorStateMatcher} from '../../../../my-error-state-matcher';
import {FormControl, Validators} from '@angular/forms';
import {User} from '../../../model/user';

@Component({
  selector: 'app-users-create-dialog',
  templateUrl: './create-dialog.html',
  styleUrls: ['./create-dialog.scss']
})
export class UsersCreateDialog implements OnInit {

  public matcher = new MyErrorStateMatcher();
  public nameFormControl = new FormControl('', [
    Validators.required
  ]);
  public usernameFormControl = new FormControl('', [
    Validators.required
  ]);
  public birthDateFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8)
  ]);
  public userPasswordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8)
  ]);
  public sexFormControl = new FormControl('', [
    Validators.required
  ]);
  public regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(this.regexEmail)
  ]);

  public user: User;

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    this.user = new User();
  }

  onNoClick(): void {
    this.dialogRef.close();
    }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
