import {Directive, HostListener, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Directive({
  selector: '[appMask]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: AppMaskDirective,
    multi: true
  }]
})

export class AppMaskDirective implements ControlValueAccessor {

  onTouched: any;
  onChange: any;

  @Input('appMask') appMask: string;

  /**
   * Write a new value to the element.
   * @param value
   */
  writeValue(value: any): void {
  }

  /**
   * Set the function to be called when the control receives a change event.
   * @param fn
   */
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  /**
   * Set the function to be called when the control receives a touch event.
   * @param fn
   */
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * método executado ao percionar uma tecla
   * @param $event
   */
  @HostListener('keyup', ['$event'])
  onKeyup($event: any) {

    let valor = $event.target.value.replace(/\D/g, '');
    const pad = this.appMask.replace(/\D/g, '').replace(/9/g, '_');
    const valorMask = valor + pad.substring(0, pad.length - valor.length);

    const BACKSPACE = 8;
    if ($event.keyCode === BACKSPACE) {
      this.onChange(valor);
      return;
    }

    if (valor.length <= pad.length) {
      this.onChange(valor);
    }

    let valorMaskPos = 0;
    valor = '';
    for (let i = 0; i < this.appMask.length; i++) {
      valor += isNaN(parseInt(this.appMask.charAt(i))) ? this.appMask.charAt(i) : valorMask[valorMaskPos++];
    }

    if (valor.indexOf('_') > -1) {
      valor = valor.substr(0, valor.indexOf('_'));
    }

    $event.target.value = valor;
  }

}
