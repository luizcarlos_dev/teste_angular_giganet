import {NgModule} from '@angular/core';
import {MaterialModule} from './material.module';
import {ProgressDialog} from '../progress-dialog/progress-dialog';
import {ProgressService} from '../services/app/progress.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppMaskDirective} from './directives/app.mask';

@NgModule({
  imports: [
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProgressDialog,
    AppMaskDirective
  ],
  entryComponents: [
    ProgressDialog
  ],
  providers: [
    ProgressService
  ],
  exports: [
    MaterialModule,
    ProgressDialog,
    FormsModule,
    ReactiveFormsModule,
    AppMaskDirective
  ]
})

export class SharedModule {
}
