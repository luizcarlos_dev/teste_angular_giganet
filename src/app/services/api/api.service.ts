import {Observable} from 'rxjs/Observable';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Injectable} from '@angular/core';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';
import {Authorization} from '../../model/authorization';

@Injectable()
class ApiService {

  protected options = new RequestOptions();
  protected http: Http;

  private _headers = new Headers();
  private _apiUri: String;

  constructor(http: Http, uri: String, authorization?: Authorization) {
    this.http = http;
    this._apiUri = uri;

    // Define options an headers with authorization
    this._headers.append('Content-Type', 'application/json');
    if (authorization) {
      this._headers.append('Authorization', authorization.token);
    }
    this.options.headers = this._headers;
  }

  /**
   * CALLER API ROUTE IN HTTP GET METHOD
   * @param route
   * @param {any} options
   * @returns {Observable<any | any>}
   */
  protected get(route: any, options: any = null) {
    return this.mapResponse(this.http.get(this._apiUri + route, options));
  }

  /**
   * CALLER API ROUTE IN HTTP POST METHOD
   * @param route
   * @param data
   * @param options
   * @returns {Observable<any | any>}
   */
  protected post(route: any, data: any, options: any = null) {
    return this.mapResponse(this.http.post(this._apiUri + route, data, options));
  }

  /**
   * CALLER API ROUTE IN HTTP PUT METHOD
   * @param route
   * @param data
   * @param options
   * @returns {Observable<any | any>}
   */
  protected put(route: any, data: any, options: any = null) {
    return this.mapResponse(this.http.put(this._apiUri + route, data, options));
  }

  /**
   * CALLER API ROUTE IN HTTP DELETE METHOD
   * @param route
   * @param options
   * @returns {Observable<any | any>}
   */
  protected del(route: any, options: any = false) {
    return this.mapResponse(this.http.delete(this._apiUri + route, options));
  }

  /**
   * Map responses using RXJS
   * @param {Observable<Response>} request
   * @returns {Observable<any | any>}
   * @public
   */
  public mapResponse(request: Observable<Response>) {
    return request
      .map(
        (res: Response) => res.json()
      ).catch(
        (err: Response) => Observable.throw(err.json())
      );
  }

}

export default ApiService;
