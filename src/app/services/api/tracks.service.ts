import ApiService from './api.service';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {environment} from '../../../environments/environment';
import {Track} from '../../model/track';
import {AuthorizationStorageService} from '../app/storage/authorization.storage';

@Injectable()
export class TracksService extends ApiService {

  constructor (http: Http, _authorizationStorageService: AuthorizationStorageService) {
    const apiUrl = environment.api.default.uri;
    super(http, apiUrl + 'track', _authorizationStorageService.getAuthorization());
  }

  /**
   * Obter todos as músicas da api
   * @param query
   * @returns {Observable<any|any>}
   */
  findAll(query?: string) {
    query = query ? '?' + query : '';
    return this.get('/' + query, this.options);
  }

  /**
   * Criar uma música nova
   * @param track
   * @returns {Observable<any|any>}
   */
  create(track: Track) {
    return this.post('/', track, this.options);
  }

  /**
   * Atualizar uma música
   * @returns {Observable<any|any>}
   * @param track
   * @param _id
   */
  update(track: Track, _id: string) {
    const updatedTrack = {...track, _id: undefined, __v: undefined};
    return this.put('/' + _id, updatedTrack, this.options);
  }

  /**
   * Remover uma música
   * @returns {Observable<any|any>}
   * @param _id
   */
  remove(_id: string) {
    return this.del('/' + _id, this.options);
  }

}

