import ApiService from './api.service';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {environment} from '../../../environments/environment';
import {Auth} from '../../model/auth';

@Injectable()
export class AuthService extends ApiService {

  constructor (http: Http) {
    const apiUrl = environment.api.default.uri;
    super(http, apiUrl + 'auth');
  }

  /**
   * Validação de login
   * @param {Auth} auth
   */
  auth(auth: Auth) {
    return this.post('/', auth, this.options);
  }


}

