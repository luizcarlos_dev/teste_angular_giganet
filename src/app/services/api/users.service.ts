import ApiService from './api.service';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {environment} from '../../../environments/environment';
import {User} from '../../model/user';
import {AuthorizationStorageService} from '../app/storage/authorization.storage';

@Injectable()
export class UsersService extends ApiService {

  constructor (http: Http, _authorizationStorageService: AuthorizationStorageService) {
    const apiUrl = environment.api.default.uri;
    super(http, apiUrl + 'user', _authorizationStorageService.getAuthorization());
  }


  /**
   * Obter todos os usuários da api
   * @param query
   * @returns {Observable<any|any>}
   */
  findAll(query?: string) {
    query = query ? '?' + query : '';
    return this.get('/' + query, this.options);
  }

  /**
   * Criar um usuário novo
   * @param user
   * @returns {Observable<any|any>}
   */
  create(user: User) {
    return this.post('/', user, this.options);
  }

  /**
   * Atualizar um usuário
   * @returns {Observable<any|any>}
   * @param user
   * @param _id
   */
  update(user: User, _id: string) {

    const updatedUser = {
      ...user,
      _id: undefined,
      __v: undefined,
      token: undefined,
      favorites: {
        tracks: [],
        albums: []
      }
    };

    const auxUser = {
      ...user
    };

    for (const track of auxUser.favorites.tracks) {
      const auxTrack = {
        ...track,
        __v: undefined
      };

      updatedUser.favorites.tracks.push(auxTrack);
    }

    for (const album of auxUser.favorites.albums) {
      const auxAlbum = {
        ...album,
        __v: undefined,
        tracks: undefined,
        year: undefined
      };

      updatedUser.favorites.albums.push(auxAlbum);
    }
    return this.put('/' + _id, updatedUser, this.options);
  }

  /**
   * Remover um usuário
   * @returns {Observable<any|any>}
   * @param _id
   */
  remove(_id: string) {
    return this.del('/' + _id, this.options);
  }

}

