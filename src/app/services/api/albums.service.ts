import ApiService from './api.service';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {environment} from '../../../environments/environment';
import {Album} from '../../model/album';
import {AuthorizationStorageService} from '../app/storage/authorization.storage';

@Injectable()
export class AlbumsService extends ApiService {

  constructor (http: Http, _authorizationStorageService: AuthorizationStorageService) {
    const apiUrl = environment.api.default.uri;
    super(http, apiUrl + 'album', _authorizationStorageService.getAuthorization());
  }

  /**
   * Obter todos os albuns da api
   * @param query
   * @returns {Observable<any|any>}
   */
  findAll(query?: string) {
    query = query ? '?' + query : '';
    return this.get('/' + query, this.options);
  }

  /**
   * Criar um album novo
   * @param album
   * @returns {Observable<any|any>}
   */
  create(album: Album) {
    const auxAlbum = {...album};
    album.tracks = [];
    for (const track of auxAlbum.tracks) {
      const auxTrack = {
        ...track,
        duration: undefined,
        __v: undefined
      };
      album.tracks.push(auxTrack);
    }
    return this.post('/', album, this.options);
  }

  /**
   * Atualizar um album
   * @returns {Observable<any|any>}
   * @param album
   * @param _id
   */
  update(album: Album, _id: string) {

    const updatedAlbum = {
      ...album,
      _id: undefined,
      __v: undefined,
      tracks: []
    };

    const auxAlbum = {...album};

    for (const track of auxAlbum.tracks) {
      const auxTrack = {
        ...track,
        duration: undefined,
        __v: undefined
      };
      updatedAlbum.tracks.push(auxTrack);
    }

    return this.put('/' + _id, updatedAlbum, this.options);
  }

  /**
   * Remover um album
   * @returns {Observable<any|any>}
   * @param _id
   */
  remove(_id: string) {
    return this.del('/' + _id, this.options);
  }

}

