import {Injectable} from '@angular/core';
import {ProgressDialog} from '../../progress-dialog/progress-dialog';
import {MatDialog, MatDialogRef} from '@angular/material';

@Injectable()
export class ProgressService {

  private _progressDialogRef: MatDialogRef<ProgressDialog>;

  constructor(private _matDialog: MatDialog) {
  }

  /**
   * Método que exibe o dialogo de aguarde entre as requisições
   * @public
   */
  openProgressDialog(): void {
    this._progressDialogRef = this._matDialog.open(ProgressDialog, {
      width: '200px',
      height: '200px',
      disableClose: true
    });
  }

  /**
   * Método que finaliza o dialogo de aguarde entre as requisições
   * @public
   */
  closeProgressDialog(): void {
    this._progressDialogRef.close();
  }

}
