import {BrowserStorageService} from './browser.storage.service';

export class AuthorizationStorageService extends BrowserStorageService {

  /**
   * Storage keys
   */
  protected storageKeys = {
    Authorization: 'authorization'
  };

  constructor() {
    super();
  }

  /**
   * Storage authorization data using cookie storage
   * @param {Object} data
   */
  public setAuthorization(data: Object) {
    this._setLocalStorage(this.storageKeys.Authorization, data);
  }

  /**
   * Return authorization data
   * @returns {any}
   */
  public getAuthorization() {
    return this._getLocalStorage(this.storageKeys.Authorization);
  }

  /** Revoke the authorization cookie
   */
  public revokeAuthorization() {
    this._deleteLocalStorage(this.storageKeys.Authorization);
  }

}
