export class BrowserStorageService {

  /**
   * Global prefix fot storage keys
   * @type {string}
   * @private
   */
  private _keyPrefix = 'teste_giganete_luiz_carlos_';

  /**
   * Store a data object in local storage
   * @param {string} key
   * @param {Object} data
   * @private
   */
  protected _setLocalStorage(key: string, data: Object) {
    try {
      localStorage.setItem(this._keyPrefix + key, btoa(JSON.stringify(data)));
    } catch (exception) {
      throw new Error('storage fail');
    }
  }

  /**
   * Return a data object of local storage
   * @param {string} key
   * @return {any}
   * @private
   */
  protected _getLocalStorage(key: string) {
    try {
      return JSON.parse(atob(localStorage.getItem(this._keyPrefix + key)));
    } catch (exception) {
      return null;
    }
  }

  /**
   * Delete data of local storage
   * @param {string} key
   * @private
   */
  protected _deleteLocalStorage(key: string) {
    try {
      localStorage.removeItem(this._keyPrefix + key);
    } catch (exception) {
      throw new Error('delete storage fail');
    }
  }

  /**
   * Store object data in session storage
   * @param {string} key
   * @param {Object} data
   * @private
   */
  protected _setSessionStorage(key: string, data: Object) {
    try {
      sessionStorage.setItem(this._keyPrefix + key, btoa(JSON.stringify(data)));
    } catch (exception) {
      throw new Error('session storage fail');
    }
  }

  /**
   * Return object data of session storage
   * @param {string} key
   * @return {any}
   * @private
   */
  protected _getSessionStorage(key: string) {
    try {
      return JSON.parse(atob(sessionStorage.getItem(this._keyPrefix + key)));
    } catch (exception) {
      return null;
    }
  }

  /**
   * Delete data of session storage
   * @param {string} key
   * @private
   */
  protected _deleteSessionStorage(key: string) {
    try {
      sessionStorage.removeItem(this._keyPrefix + key);
    } catch (exception) {
      throw new Error('delete session storage fail');
    }
  }

}
