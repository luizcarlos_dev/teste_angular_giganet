import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {AuthorizationStorageService} from './storage/authorization.storage';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthGuardService implements CanActivate {

  /**
   * redirecionar usuário para a tela de login
   * @returns {any}
   */
  static redirectToLogin() {
    window.location.href = environment.app.default.uri  + 'login/';
    return Observable.of(false);
  }

  constructor(private _authorizationStorageService: AuthorizationStorageService) {
  }

  /**
   * método que identifica se o usuario terá permissão de acesso
   * @param next
   * @param state
   * @returns {any}
   */
  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const localAuthorization = this._authorizationStorageService.getAuthorization();

    if (!localAuthorization) {
      return AuthGuardService.redirectToLogin();
    }

    return true;
  }

}
