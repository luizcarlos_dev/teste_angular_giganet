import {Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthGuardService} from './services/app/auth.guard.service';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'app',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        canActivate: [AuthGuardService],
        path: 'app',
        loadChildren: './drawer/drawer.module#DrawerModule'
      },
    ]
  }
];
