import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {AppRoutes} from './app.routes';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpModule} from '@angular/http';
import {LoginComponent} from './login/login.component';
import {AuthService} from './services/api/auth.service';
import {AuthGuardService} from './services/app/auth.guard.service';
import {AuthorizationStorageService} from './services/app/storage/authorization.storage';
import {SharedModule} from './shared/shared.module';

@NgModule({
  entryComponents: [
  ],
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    BrowserAnimationsModule,
    HttpModule
  ],
  providers: [
    AuthGuardService,
    AuthorizationStorageService,
    AuthService
  ],

  bootstrap: [AppComponent]
})

export class AppModule { }
