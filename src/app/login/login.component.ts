import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Auth} from '../model/auth';
import {AuthService} from '../services/api/auth.service';
import {MatSnackBar} from '@angular/material';
import {AuthorizationStorageService} from '../services/app/storage/authorization.storage';
import {ProgressService} from '../services/app/progress.service';
import {Authorization} from '../model/authorization';
import * as crypto from 'crypto-js';

@Component({
  selector: 'app-login',
  providers: [],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public auth = new Auth;

  constructor(private _router: Router,
              private _authorizationStorageService: AuthorizationStorageService,
              private _snackBar: MatSnackBar,
              private _progressService: ProgressService,
              private _authService: AuthService) {
  }

  ngOnInit() {

  }

  /**
   * ...
   * @public
   */
  public onSubmit() {

    this._progressService.openProgressDialog();

    const auth = {...this.auth, password: crypto.SHA256(this.auth.password).toString(crypto.enc.Base64)};
    this._authService.auth(auth)
      .subscribe(
        res => {
          this._progressService.closeProgressDialog();

          const authorization = new Authorization(res.data._id, res.data.email, res.data.token);

          this._authorizationStorageService.setAuthorization(authorization);

          this._router.navigate(['/app']);
        },
        err => {
          this._progressService.closeProgressDialog();

          this._snackBar.open('Email ou senha inválido(a)!', 'FECHAR', {
            duration: 5000,
          });
        }
      );
  }


}
