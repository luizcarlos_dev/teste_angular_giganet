import {Track} from './track';

export class Album {

  _id?: string;
  name?: string;
  year?: number;
  artist?: string;
  tracks?: Track[];

  constructor(_id?: string, name?: string, year?: number, artist?: string, tracks?: Track[]) {
    this._id = _id;
    this.name = name;
    this.year = year;
    this.artist = artist;
    this.tracks = tracks;
  }

}
