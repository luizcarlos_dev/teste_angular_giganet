export class Track {

  _id?: string;
  name?: string;
  duration?: number;

  constructor(_id?: string, name?: string, duration?: number) {
    this._id = _id;
    this.name = name;
    this.duration = duration;
  }

}
