export class Authorization {

  _id?: string;
  email?: string;
  token?: string;

  constructor(id: string, email: string, token: string) {
    this._id = id;
    this.email = email;
    this.token = token;
  }

}
