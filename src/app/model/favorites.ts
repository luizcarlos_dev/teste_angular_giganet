import {Track} from './track';
import {Album} from './album';

export class Favorites {

  tracks?: Track[];
  albums?: Album[];

  constructor(tracks: Track[], albums: Album[]) {
    this.tracks = tracks;
    this.albums = albums;
  }

}
