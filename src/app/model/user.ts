import {Favorites} from './favorites';

export class User {

  _id?: string;
  token?: string;
  email?: string;
  password?: string;
  name?: string;
  birthDate?: string;
  sex?: string;
  username?: string;
  favorites?: Favorites;

  constructor(id?: string, token?: string, email?: string, password?: string, name?: string, birthDate?: string,
              sex?: string, username?: string, favorites?: Favorites) {
    this._id = id;
    this.token = token;
    this.email = email;
    this.password = password;
    this.name = name;
    this.birthDate = birthDate;
    this.sex = sex;
    this.username = username;
    this.favorites = favorites;
  }

}
