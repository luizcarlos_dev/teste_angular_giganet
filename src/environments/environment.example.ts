export const environment = {

  production: false,

  api: {
    default: {
      uri: 'http://192.168.0.10:9595/'
    }
  },
  app: {
    default: {
      uri: 'http://192.168.0.10:8585/'
    }
  }

};
