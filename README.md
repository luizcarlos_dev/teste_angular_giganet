# Sobre

Teste - Angular5 - GigaNet - Luiz Carlos

# Instruções

```
git clone https://luizcarlos_dev@bitbucket.org/luizcarlos_dev/teste_angular_giganet.git
```

```
cd teste_angular_giganet
```

# Ajustar ambiente

```
cp src/environments/environment.example.ts src/environments/environment.ts
cp src/environments/environment.example.ts src/environments/environment.prod.ts
```

# Substituir certificados ssl (caso for usar ssl)

```
cp certificates/fullchain-example.crt certificates/fullchain.crt
cp certificates/privkey-example.key certificates/privkey.key
```

# Ajustar ambiente nginx (caso for usar container docker)

```
cp -rvf nginx/conf.d.example/ nginx/conf.d
```

# Executar em modo de desenvolvimento

```
npm install
node_modules/@angular/cli/bin/ng serve --host HOST --port PORT --ssl 1 --ssl-key "certificates/privkey.key" --ssl-cert "certificates/fullchain.crt";
```

# Executar em modo de produção
```
npm install
node_modules/@angular/cli/bin/ng serve --host HOST --port PORT --env=prod --ssl 1 --ssl-key "certificates/privkey.key" --ssl-cert "certificates/fullchain.crt";
```

#Executar em um container docker (requer docker compose)

```
npm run deploy
```
