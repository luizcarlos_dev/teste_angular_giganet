FROM node:stretch

# Create app directory
RUN mkdir -p /usr/share/nginx/html/
WORKDIR /usr/share/nginx/html/

#RUN apk add --update nginx
RUN apt-get update
RUN apt-get install nginx -y

# Start service
EXPOSE 80
EXPOSE 443
CMD [ "sh" ]
